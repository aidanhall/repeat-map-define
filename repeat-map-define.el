;;; repeat-map-define.el --- Define a new repeat map easily.  -*- lexical-binding: t; -*-

;; Copyright (C) 2022 Aidan Hall

;; Author: Aidan Hall <aidan.hall202@gmail.com>
;; Keywords: repeat
;; Version: 0.0.5
;; Requires: emacs-28.1
;; URL: https://gitlab.com/aidanhall/repeat-map-define

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides the `repeat-map-define' macro,
;; which makes it easier to define repeat maps using the built-in `repeat' package.
;; This provides a simpler alternative to packages like `hydra' and `transient',
;; which should work decently well for a single user.
;;
;; E.g.
;; (repeat-map-define
;;  defun-repeat-map
;;  ("a" beginning-of-defun)
;;  ("e" end-of-defun))
;;
;; The repeat map itself can be bound to a key to enable the repeating bindings
;; without performing one of the associated functions:
;;
;; (global-set-key (kbd "C-x w") defun-repeat-map)
;;
;; Please see the help for the function, as well as my blog post about it:
;; https://aidanhall.gitlab.io/website/blog/22-05-17_21.46.html

;;; Code:

(require 'repeat)

(defun repeat-map--create (binds)
  "Create a keymap using BINDS.
See `repeat-map-define' for the required format."
  (let ((map (make-sparse-keymap)))
    (dolist (m binds)
      (let ((key (car m)) (command (cadr m)))
	(define-key map key command)))
    map))

(defun repeat-map--register (name binds)
  "Register the functions from BINDS with the repeat map NAME.
If any of these functions are called, the repeat map will start."
  (dolist (m binds)
    (put (cadr m) 'repeat-map name)))

(defmacro repeat-map-define (name &rest binds)
  "Create a new keymap called NAME, using BINDS.
The BINDS argument is a list of lists like this:
\(key function\)
After that, if `repeat-mode' is enabled and a key bound to one of the functions
in BINDS is pressed, the repeat map will be started."
  `(let* ((processed-binds
           ',(mapcar
              (lambda (bind)
                (cons
                 (if (stringp (car bind))
                     (kbd (car bind))
                   (car bind))
                 (cdr bind)))
              binds)))
     (repeat-map--register
      (defvar ,name
        (repeat-map--create processed-binds))
      processed-binds)))

(provide 'repeat-map-define)

;;; repeat-map-define.el ends here
